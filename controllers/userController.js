App.UserController = Ember.ObjectController.extend({
  // the deleteMode property is false by default
  deleteMode: false,

  actions: {
    delete: function() {
      // our delete method now only toggles delete mode's value
      this.toggleProperty('deleteMode');
    },
    cancelDelete: function() {
      // set deleteMode back to false
      this.set('deleteMode', false);
    },
    confirmDelete: function(){
      // this tells Ember-Data to delete the current user
      this.get('model').deleteRecord();
      this.get('model').save();
      // thne go to the users route
      this.transitionToRoute('users');
      // set deleteMode back to false
      this.set('deleteMode', false);
    },
    // edit mode remains the same
    edit: function() {
      this.transitionToRoute('user.edit');
    }
  }
});